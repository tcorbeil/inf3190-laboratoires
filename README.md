# INF3190-Laboratoires

Exercices de web complétés en laboratoire

## Solutions des laboratoires

1. Laboratoire 05R01: [Solution](https://gitlab.com/tcorbeil/inf3190-laboratoires/blob/master/LAB05R01.zip)
2. Laboratoire 07R01: [Solution](https://gitlab.com/tcorbeil/inf3190-laboratoires/-/blob/master/LAB07R01.zip)
3. Laboratoire 8: [Solution](https://gitlab.com/tcorbeil/inf3190-laboratoires/-/blob/master/LAB08.zip)
4. Laboratoire 6: [Solution](https://gitlab.com/tcorbeil/inf3190-laboratoires/-/blob/master/LAB6.zip)
5. Laboratoire 10R01: [Solution](https://gitlab.com/tcorbeil/inf3190-laboratoires/-/blob/master/LAB10R01.zip)
6. Laboratoire 10R03 (Partie JSON): [Solution](https://gitlab.com/tcorbeil/inf3190-laboratoires/-/blob/master/universites.json)
7. Laboratoire 16: ([Code](https://gitlab.com/tcorbeil/inf3190-laboratoires/-/blob/master/LAB16/INF3190LAB16.zip)) ([Vidéo](https://gitlab.com/tcorbeil/inf3190-laboratoires/-/blob/master/LAB16/LAB_16_VIDEO.zip))